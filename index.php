<?php

$searchWords = ['php','html','интернет','Web'];

$searchStrings = [
    'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
	'PHP - это распространенный язык программирования с открытым исходным кодом.',
	'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'
    ];

function wordSearcher($strings, $words)
{
    $matches = [];

    foreach ($words as $word){
        if (preg_match('/' . $word . '/ui', $strings)){
            $matches[] = $word;
        }
    }
    return $matches;
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Test page</title>
		<meta charset="utf-8">
		<meta name="description" content="Homework 19 Test Page">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<div id="header">
				<h1>Homework 19</h1>
				<h2>Test page</h2>
			</div>
			<div id="content">
                <?php foreach ($searchStrings as $key => $string) : ?>
                    <?php $match = wordSearcher($string, $searchWords) ?>
                    <?php if (!empty($match)): ?>
                        <p>В предложении №<?=$key+1 ?> есть слова: <?= implode(', ', $match)?>.</p>
                    <?php else :?>
                        <p>В предложении №<?=$key+1 ?> совпадений нет.</p>
                    <?php endif ?>
                <?php endforeach ?>
			</div>
			<div id="footer">
				<h6>Школа Hillel. Stas Chyrkov (с) 2019</h6>
			</div>
		</div>
	</body>
</html>

